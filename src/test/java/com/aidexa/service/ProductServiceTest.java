package com.aidexa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.aidexa.controller.ApiConsumerController;
import com.aidexa.controller.dto.CategoryDTO;
import com.aidexa.controller.dto.ProductDTO;
import com.aidexa.model.Product;

@SpringBootTest
class ProductServiceTest {

	@MockBean
	private ApiConsumerController ApiConsumerController;

	@Autowired
	private ApiConsumerService apiConsumerService;

	@Autowired
	private ProductService productService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void exist_two_products() {
		storeProducts();
		System.out.println("test-------------------------->>>>>");

		Pageable pageable = PageRequest.of(0, 20);
		Page<Product> products = productService.findAll(pageable);

		assertEquals(2, products.getTotalElements());
		;

	}

	@Test
	public void exist_one_product_for_category_1() {

		int idCategory = 1;
		Pageable pageable = PageRequest.of(0, 20);
		Page<Product> products = productService.findAllByIdCategory(idCategory, pageable);
		assertEquals(1, products.getTotalElements());

		;

	}

	private void storeProducts() {
		CategoryDTO category1 = new CategoryDTO();
		category1.setId(1);
		category1.setName("first category");

		CategoryDTO category2 = new CategoryDTO();
		category2.setId(2);
		category2.setName("second category");

		ProductDTO product1 = new ProductDTO();
		product1.setId(1);
		product1.setName("product name 1");
		product1.setDescription("product description 1");
		product1.setPrice("10.00");
		product1.setDiscount_amount("9.55");
		product1.setImage("https://loremflickr.com/250/250");
		product1.setStatus(true);

		List<CategoryDTO> categoriesP1 = new ArrayList<CategoryDTO>();
		categoriesP1.add(category1);
		categoriesP1.add(category2);
		product1.setCategories(categoriesP1);

		ProductDTO product2 = new ProductDTO();
		product2.setId(2);
		product2.setName("product name 2");
		product2.setDescription("product description 2");
		product2.setPrice("10.00");
		product2.setDiscount_amount("9.55");
		product2.setImage("https://loremflickr.com/250/250");
		product2.setStatus(true);

		List<CategoryDTO> categoriesP2 = new ArrayList<CategoryDTO>();
		categoriesP2.add(category2);
		product2.setCategories(categoriesP2);

		List<ProductDTO> products = new ArrayList<ProductDTO>();
		products.add(product1);
		products.add(product2);

		apiConsumerService.createProductsAndCategories(products);
	}

}
