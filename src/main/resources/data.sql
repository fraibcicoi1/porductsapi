DROP TABLE IF EXISTS product_category;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS categories;

CREATE TABLE products (
    id INT PRIMARY KEY,
    product_name VARCHAR(250) NOT NULL,
    product_description VARCHAR(2048) NOT NULL,
    product_image VARCHAR(250) NOT NULL,
    price DECIMAL(20, 2) NOT NULL,
    discount_amount DECIMAL(20, 2) NOT NULL,
    product_status BOOLEAN NOT NULL
);

CREATE TABLE categories (
    id INT PRIMARY KEY,
    category_name VARCHAR(250) NOT NULL
);

CREATE TABLE product_category (
	idProduct INT NOT NULL, 
	idCategory INT NOT NULL,
	FOREIGN KEY (idProduct) REFERENCES products(id),
	FOREIGN KEY (idCategory) REFERENCES categories(id),
	PRIMARY KEY (idProduct, idCategory)
);