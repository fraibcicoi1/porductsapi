package com.aidexa.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.aidexa.model.ProductCategory;

@RepositoryRestResource(path = "product-category", collectionResourceRel = "product-category")
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

	List<ProductCategory> findAllById_IdCategory(int idCategory, Pageable pageable);

}