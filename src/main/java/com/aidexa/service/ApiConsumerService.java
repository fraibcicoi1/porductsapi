package com.aidexa.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aidexa.controller.dto.CategoryDTO;
import com.aidexa.controller.dto.ProductDTO;
import com.aidexa.model.Category;
import com.aidexa.model.Product;
import com.aidexa.model.ProductCategory;
import com.aidexa.model.ProductCategoryKey;
import com.aidexa.repository.CategoryRepository;
import com.aidexa.repository.ProductCategoryRepository;
import com.aidexa.repository.ProductRepository;

@Service
public class ApiConsumerService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	public ApiConsumerService() {

	}

	/**
	 * iterate over a list of products with some categories associated, store the
	 * product, the first time store the category and store the relation between
	 * product and category
	 */
	public boolean createProductsAndCategories(List<ProductDTO> productsDTO) {

		for (ProductDTO productDTO : productsDTO) {
			saveProductFromDTO(productDTO);

			for (CategoryDTO categoryDTO : productDTO.getCategories()) {
				Optional<Category> category = this.categoryRepository.findById(categoryDTO.getId());

				if (category.isEmpty()) {
					saveCategoryFromDTO(categoryDTO);
				}

				saveProductCategoryFromDTO(productDTO, categoryDTO);
			}

		}
		return true;

	}

	private ProductCategory saveProductCategoryFromDTO(ProductDTO productDTO, CategoryDTO categoryDTO) {
		ProductCategoryKey productCategoryKey = new ProductCategoryKey();
		productCategoryKey.setIdCategory(categoryDTO.getId());
		productCategoryKey.setIdProduct(productDTO.getId());
		ProductCategory productCategory = new ProductCategory();
		productCategory.setId(productCategoryKey);
		return productCategoryRepository.save(productCategory);
	}

	private Category saveCategoryFromDTO(CategoryDTO categoryDTO) {
		Category newCategory = new Category();
		newCategory.setId(categoryDTO.getId());
		newCategory.setName(categoryDTO.getName());
		return categoryRepository.save(newCategory);
	}

	private Product saveProductFromDTO(ProductDTO productDTO) {
		Product product = new Product();
		product.setId(productDTO.getId());
		product.setName(productDTO.getName());
		product.setDescription(productDTO.getDescription());
		product.setImage(productDTO.getImage());
		product.setStatus(productDTO.getStatus());
		product.setPrice(new BigDecimal(productDTO.getPrice()));
		product.setDiscount_amount(new BigDecimal(productDTO.getDiscount_amount()));
		return productRepository.save(product);
	}

}
