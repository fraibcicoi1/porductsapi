package com.aidexa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.aidexa.model.Product;
import com.aidexa.model.ProductCategory;
import com.aidexa.repository.ProductCategoryRepository;
import com.aidexa.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	public ProductService() {

	}

	public Page<Product> findAll(Pageable pageable) {
		Page<Product> products = productRepository.findAll(pageable);
		return products;
	}

	public Page<Product> findAllByIdCategory(int categoryId, Pageable pageable) {
		List<Product> products = productCategoryRepository.findAllById_IdCategory(categoryId, pageable).stream()
				.map(ProductCategory::getProduct).collect(Collectors.toList());

		return new PageImpl<>(products);
	}

}
