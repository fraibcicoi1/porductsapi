package com.aidexa.controller;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.aidexa.controller.dto.ApiDTO;
import com.aidexa.service.ApiConsumerService;

@RestController
public class ApiConsumerController {

	private final String API_URI = "https://gorest.co.in/public-api/products?page={?}";
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiConsumerService apiConsumerService;

	public ApiConsumerController() {
	}

	/**
	 * Consumes all the pages of the products API to store all the data in the
	 * database
	 */
	@PostConstruct
	private void consumeProducts() {
		RestTemplate restTemplate = new RestTemplate();
		LOG.info("Starting to retrieving data");

		ApiDTO firstPage = restTemplate.getForObject(API_URI, ApiDTO.class, 1);
		this.apiConsumerService.createProductsAndCategories(firstPage.getData());

		for (int i = 2; i < firstPage.getMeta().getPagination().getPages(); i++) {
			ApiDTO page = restTemplate.getForObject(API_URI, ApiDTO.class, i);
			this.apiConsumerService.createProductsAndCategories(page.getData());
		}
		LOG.info("All data are stored");

	}

}
