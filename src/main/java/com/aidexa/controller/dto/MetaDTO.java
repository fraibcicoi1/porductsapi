package com.aidexa.controller.dto;

public class MetaDTO {
	private PaginationDTO pagination;

	public MetaDTO() {
	}

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}
}
