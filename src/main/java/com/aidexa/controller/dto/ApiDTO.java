package com.aidexa.controller.dto;

import java.io.Serializable;
import java.util.List;

public class ApiDTO implements Serializable {
	private int code;
	private List<ProductDTO> data;
	private MetaDTO meta;

	public ApiDTO() {

	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<ProductDTO> getData() {
		return data;
	}

	public void setData(List<ProductDTO> data) {
		this.data = data;
	}

	public MetaDTO getMeta() {
		return meta;
	}

	public void setMeta(MetaDTO meta) {
		this.meta = meta;
	}

}
