package com.aidexa.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aidexa.model.Product;
import com.aidexa.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductService productService;

	public ProductController() {
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Product>> getAllByCategory(Pageable pageable) {
		LOG.info("GET: /products");
		return ResponseEntity.ok(productService.findAll(pageable));
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { "categoryId" })
	public ResponseEntity<Page<Product>> getAll(
			@RequestParam(value = "categoryId", required = false) Integer categoryId, Pageable pageable) {
		LOG.info("GET: /products?categoryId=?");

		return ResponseEntity.ok(productService.findAllByIdCategory(categoryId, pageable));
	}

}
