package com.aidexa.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProductCategoryKey implements Serializable {

	@Column(name = "idproduct")
	private int idProduct;

	@Column(name = "idcategory")
	private int idCategory;

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ProductCategoryKey that = (ProductCategoryKey) o;
		return Objects.equals(idProduct, that.idProduct) && Objects.equals(idCategory, that.idCategory);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idProduct, idCategory);
	}

}
