package com.aidexa.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.springframework.data.rest.core.annotation.RestResource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "product_category")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategory implements Serializable {
	@EmbeddedId
	@RestResource(exported = false)
	ProductCategoryKey id;

	@ManyToOne
	@MapsId("idproduct")
	@JoinColumn(name = "idproduct", insertable = false, updatable = false)
	@JsonProperty("product")
	private Product product;

	@ManyToOne
	@MapsId("idcategory")
	@JoinColumn(name = "idcategory", insertable = false, updatable = false)
	@JsonProperty("category")
	private Category category;

	public ProductCategoryKey getId() {
		return id;
	}

	public void setId(ProductCategoryKey id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
