package com.aidexa.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "products")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {

	@Id
	@JsonProperty("id")
	private int id;

	@Column(name = "product_name")
	@JsonProperty("name")
	private String name;

	@Column(name = "product_description")
	@JsonProperty("description")
	private String description;

	@Column(name = "product_image")
	@JsonIgnore
	private String image;

	@Column(name = "Price")
	@JsonProperty("price")
	private BigDecimal price;

	@Column(name = "product_status")
	@JsonProperty("status")
	private Boolean status;

	@Column(name = "discount_amount")
	@JsonProperty("discount_amount")
	private BigDecimal discount_amount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public BigDecimal getDiscount_amount() {
		return discount_amount;
	}

	public void setDiscount_amount(BigDecimal discount_amount) {
		this.discount_amount = discount_amount;
	}

	@JsonGetter("image")
	public Map<String, String> getTheName() {
		Map<String, String> imageMap = new HashMap<String, String>();
		imageMap.put("src", image);
		return imageMap;
	}

}
