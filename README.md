## General info
This project is simple application that read the products from [public API](https://gorest.co.in/public-api/products) and store them in a H2 database.
	
## Technologies
Project is created with:
* Spring boot 2.4.4

	
## Build
To run this project, build it locally using maven:

```
$ mvn clean package
```

## API
Project exposes the following API:
* GET: http://localhost:8081/my-api/products for a paginated list of products
* GET: http://localhost:8081/my-api/products?categoryId=? for a paginated list of products filtered by idCategory


